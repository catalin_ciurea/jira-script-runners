/**
 * @author: Catalin Ciurea
 * @date: 16/04/2016
 * IMPORTANT: check uniqueness of the schemes else you'll get same name /different ID schemes.
 */

import com.atlassian.jira.bc.issue.fields.screen.FieldScreenService
import com.atlassian.jira.bc.project.ProjectService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.config.ConstantsManager
import com.atlassian.jira.issue.fields.screen.*
import com.atlassian.jira.issue.fields.screen.issuetype.*
import com.atlassian.jira.project.Project
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.user.util.UserManager
import org.ofbiz.core.entity.GenericValue

//===== SERVICES ======================================
UserManager userManager = ComponentAccessor.userManager
ProjectService projectService = ComponentAccessor.getComponent(ProjectService.class)

IssueTypeScreenSchemeManager issueTypeScreenSchemeManager = ComponentAccessor.issueTypeScreenSchemeManager
FieldScreenSchemeManager fieldScreenSchemeManager = ComponentAccessor.getComponent(FieldScreenSchemeManager.class)
FieldScreenManager fieldScreenManager = ComponentAccessor.fieldScreenManager
ConstantsManager constantsManager = ComponentAccessor.constantsManager
FieldScreenService fieldScreenService = ComponentAccessor.getComponent(FieldScreenService.class)

//===============================================================

def PREFIX = "ZEUS: "
ApplicationUser rootUser = userManager.getUserByName("root");
Project project = projectService.getProjectByKey(rootUser, "DEVY").getProject()

Collection<IssueTypeScreenScheme> allIssueTypeScreenSchemes = issueTypeScreenSchemeManager.getIssueTypeScreenSchemes()
IssueTypeScreenScheme projectIssueTypeScheme = issueTypeScreenSchemeManager.getIssueTypeScreenScheme(project)

// check uniqueness of scheme name
def targetIssueTypeScreenSchemeName = "${PREFIX} ${projectIssueTypeScheme.name}".toString()
for (eachIssueTypeScreenScheme in allIssueTypeScreenSchemes) {
    if (eachIssueTypeScreenScheme.name.equals(targetIssueTypeScreenSchemeName)) {
        targetIssueTypeScreenSchemeName = "\$\$${PREFIX} ${projectIssueTypeScheme.name}"
        break
    }
}

//================================================================
// ISSUE TYPE SCREEN SCHEME
IssueTypeScreenScheme copyIssueTypeScreenScheme = new IssueTypeScreenSchemeImpl(issueTypeScreenSchemeManager, null);
copyIssueTypeScreenScheme.setName(targetIssueTypeScreenSchemeName);
copyIssueTypeScreenScheme.setDescription(projectIssueTypeScheme.description);


def FIELD_SCREEN_SCHEME_CACHE = [:] as HashMap<String, FieldScreenScheme>
def FIELD_SCREEN_CACHE = [:] as HashMap<String, FieldScreen>

// Copy scheme entities
for (IssueTypeScreenSchemeEntity issueTypeScreenSchemeEntity : projectIssueTypeScheme.entities) {
    FieldScreenScheme originalFieldScreenScheme = issueTypeScreenSchemeEntity.fieldScreenScheme

    // COPY ISSUE TYPE SCREEN SCHEME
    IssueTypeScreenSchemeEntity copyIssueTypeScreenSchemeEntity = new IssueTypeScreenSchemeEntityImpl(issueTypeScreenSchemeManager, (GenericValue) null, fieldScreenSchemeManager, constantsManager);
    // in Java do the null check for issueType (null means all issue types). Here groovy magic ?. works
    copyIssueTypeScreenSchemeEntity.setIssueTypeId(issueTypeScreenSchemeEntity.getIssueTypeObject()?.getId());

    //================================================================
    // COPY FIELD SCREEN SCHEME
    String targetFieldScreenSchemeName = "${PREFIX} ${originalFieldScreenScheme.name}"

    // IMPORTANT: check cache guard for trying to copy the previously created if same issue type points to a scheme
    if (targetFieldScreenSchemeName in FIELD_SCREEN_SCHEME_CACHE.keySet()) {
        FieldScreenScheme copyFieldScreenScheme = FIELD_SCREEN_SCHEME_CACHE.get(targetFieldScreenSchemeName)
        copyIssueTypeScreenSchemeEntity.setFieldScreenScheme(copyFieldScreenScheme);
        copyIssueTypeScreenScheme.addEntity(copyIssueTypeScreenSchemeEntity);
        continue
    }

    FieldScreenScheme copyFieldScreenScheme = new FieldScreenSchemeImpl(fieldScreenSchemeManager, (GenericValue) null);

    // TODO: find a way to generate a unique name or build a report for user.
    Collection<FieldScreenScheme> allFieldScreenSchemes = fieldScreenSchemeManager.getFieldScreenSchemes()
    for (eachFieldScreenScheme in allFieldScreenSchemes) {
        if (eachFieldScreenScheme.name.equals(targetFieldScreenSchemeName)) {
            // stupid hack
            targetFieldScreenSchemeName = "\$\$${PREFIX} ${originalFieldScreenScheme.name}"
            break
        }
    }

    copyFieldScreenScheme.setName(targetFieldScreenSchemeName);
    copyFieldScreenScheme.setDescription(originalFieldScreenScheme.description);

    // COPY FIELD SCREENS AND MAP THEM
    for (FieldScreenSchemeItem fieldScreenSchemeItem : originalFieldScreenScheme.getFieldScreenSchemeItems()) {
        FieldScreen originalFieldScreen = fieldScreenSchemeItem.fieldScreen

        FieldScreenSchemeItem copyFieldScreenSchemeItem = new FieldScreenSchemeItemImpl(fieldScreenSchemeManager, fieldScreenSchemeItem, fieldScreenManager)
        String targetFieldScreenName = "${PREFIX} ${originalFieldScreen.name}"

        // IMPORTANT: check cache guard for trying to copy the previously created if same issue type points to a scheme
        if (targetFieldScreenName in FIELD_SCREEN_CACHE.keySet()) {
            FieldScreen copyFieldScreen = FIELD_SCREEN_CACHE.get(targetFieldScreenName)
            copyFieldScreenSchemeItem.setFieldScreen(copyFieldScreen)
            copyFieldScreenScheme.addFieldScreenSchemeItem(copyFieldScreenSchemeItem)
            continue
        }

        // TODO: find a way to generate a unique name or build a report for user.
        Collection<FieldScreen> allFieldScreens = fieldScreenManager.getFieldScreens()
        for (eachFieldScreen in allFieldScreens) {
            if (eachFieldScreen.name.equals(targetFieldScreenName)) {
                // stupid hack
                targetFieldScreenName = "\$\$${PREFIX} ${originalFieldScreen.name}"
                break
            }
        }

        FieldScreen copyFieldScreen = fieldScreenService.copy(originalFieldScreen, targetFieldScreenName,
                originalFieldScreen.description, rootUser).get();
        copyFieldScreenSchemeItem.setFieldScreen(copyFieldScreen)
        copyFieldScreenScheme.addFieldScreenSchemeItem(copyFieldScreenSchemeItem);

        FIELD_SCREEN_CACHE.put(targetFieldScreenName, copyFieldScreen)
    }

    // ASSOCIATE FIELD SCREEN WITH ISSUE TYPE SCREEN SCHEME ENTITY
    copyIssueTypeScreenSchemeEntity.setFieldScreenScheme(copyFieldScreenScheme);

    // ASSOCIATE SCREEN SCHEME WITH ISSUE TYPE
    copyIssueTypeScreenScheme.addEntity(copyIssueTypeScreenSchemeEntity);

    // add fieldscreenscheme to cache
    FIELD_SCREEN_SCHEME_CACHE.put(targetFieldScreenSchemeName, copyFieldScreenScheme)
}

issueTypeScreenSchemeManager.addSchemeAssociation(project, copyIssueTypeScreenScheme)
copyIssueTypeScreenScheme.store();
