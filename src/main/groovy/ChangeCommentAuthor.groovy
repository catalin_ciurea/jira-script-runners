/**
 * @author: Catalin Ciurea
 * @date: 16/04/2016
 * @note: Depends on Script Runner & it is supposed to be run in the Script Console in Jira. <br />
 *        Tested with Jira v6.4.13 and Script Runner v3.0.16
 */

import com.atlassian.jira.bc.issue.comment.CommentService
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventType
import com.atlassian.jira.exception.DataAccessException
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.comments.Comment
import com.atlassian.jira.issue.comments.CommentManager
import com.atlassian.jira.issue.history.ChangeItemBean
import com.atlassian.jira.issue.index.IssueIndexManager
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.issue.util.IssueUpdateBean
import com.atlassian.jira.issue.util.IssueUpdater
import com.atlassian.jira.ofbiz.OfBizDelegator
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.user.ApplicationUsers
import com.atlassian.jira.user.util.UserManager
import com.atlassian.jira.web.bean.PagerFilter
import com.google.common.collect.ImmutableList
import org.ofbiz.core.entity.GenericEntityException
import org.ofbiz.core.entity.GenericValue
import org.ofbiz.core.util.UtilDateTime

import java.sql.Timestamp

CommentManager commentManager = ComponentAccessor.getCommentManager()
CommentService commentService = ComponentAccessor.getComponent(CommentService.class)

UserManager userManager = ComponentAccessor.getUserManager()
IssueManager issueManager = ComponentAccessor.issueManager


IssueIndexManager issueIndexManager = ComponentAccessor.issueIndexManager
IssueUpdater issueUpdater = ComponentAccessor.getComponent(IssueUpdater.class)
OfBizDelegator delegator = ComponentAccessor.ofBizDelegator

ApplicationUser rootUser = userManager.getUserByName("root");
ArrayList<ApplicationUser> oldUsers = userManager.getAllApplicationUsers().findAll { it.name =~ /_old$/ }


println "\n\n==============================================================="

for (ApplicationUser eachOldUser in oldUsers) {
    String newUsername = (eachOldUser.username =~ /(.*)_old/)[0][1]
    ApplicationUser newUser = userManager.getUserByName(newUsername);

    if (newUser == null) {
        continue
    }

    // NOTE: this depends on Script Runner plugin. Jira Comment Toolbox has its own way of searching for issues commented by someone.
    String jqlSearch = "issueFunction in commented('by ${eachOldUser.getUsername()}')"

    SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
    SearchService.ParseResult parseResult = searchService.parseQuery(ApplicationUsers.toDirectoryUser(rootUser), jqlSearch)

    if (!parseResult.isValid()) {
        println "INVALID jql Search: ${jqlSearch}"
        continue
    }

    SearchResults searchResult = searchService.search(ApplicationUsers.toDirectoryUser(rootUser), parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
    if (searchResult?.total == 0) {
        println "NO comments by author [ user: ${eachOldUser.username}, key: ${eachOldUser.key} ]"
        continue
    }

    int userCommentCounter = 0;

    for (Issue commentedIssue in searchResult.issues) {
        List<Comment> commentsForOldUser = commentService.getCommentsForUser(eachOldUser, commentedIssue);

        for (Comment comment : commentsForOldUser) {
            if (!comment.getAuthorApplicationUser().equals(eachOldUser)) {
                continue
            }
            // change the comment author
            Timestamp now = UtilDateTime.nowTimestamp()

            try {
                GenericValue newComment = delegator.findById("Action", comment.id);

                newComment.setString("author", newUser.getUsername());
                newComment.setString("updateauthor", rootUser.getUsername());

                newComment.set("updated", now);
                newComment.store();
            } catch (GenericEntityException var11) {
                throw new DataAccessException(var11);
            }

            // index comment
            Comment commentById = commentManager.getCommentById(comment.id)
            issueIndexManager.reIndexComments(Arrays.asList(commentById))

            // Add changelog info about this
            MutableIssue mutableIssue = issueManager.getIssueByCurrentKey(commentedIssue.key)
            ChangeItemBean changeItemBean = new ChangeItemBean("custom", "Comment Author", eachOldUser.getDisplayName(), "", newUser.getDisplayName(), "", now)
            IssueUpdateBean issueUpdateBean = new IssueUpdateBean(mutableIssue, mutableIssue, EventType.ISSUE_COMMENT_EDITED_ID, rootUser);
            issueUpdateBean.setChangeItems(ImmutableList.of(changeItemBean));
            issueUpdater.doUpdate(issueUpdateBean, true);
            userCommentCounter++;
        }
    }

    println "[${eachOldUser}] => [${newUser}] : ${userCommentCounter} comments"
}