/**
 * @author: Catalin Ciurea
 * @date: 16/04/2016
 * @note: Depends on Script Runner & it is supposed to be run in the Script Console in Jira. <br />
 *        Tested with Jira v6.4.13 and Script Runner v3.0.16
 */


import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.bc.issue.search.SearchService
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.issue.search.SearchResults
import com.atlassian.jira.user.ApplicationUser
import com.atlassian.jira.user.ApplicationUsers
import com.atlassian.jira.user.util.UserManager
import com.atlassian.jira.web.bean.PagerFilter

UserManager userManager = ComponentAccessor.getUserManager()
IssueService issueService = ComponentAccessor.issueService

ApplicationUser rootUser = userManager.getUserByName("root");
ArrayList<ApplicationUser> oldUsers = userManager.getAllApplicationUsers().findAll { it.name =~ /_old$/ }


println "\n\n==============================================================="
for (ApplicationUser eachOldUser in oldUsers) {
    String newUsername = (eachOldUser.username =~ /(.*)_old/)[0][1]
    ApplicationUser newUser = userManager.getUserByName(newUsername);

    if (newUser == null) {
        continue
    }

    String jqlSearch = "assignee = ${eachOldUser.getUsername()}"

    SearchService searchService = ComponentAccessor.getComponent(SearchService.class)
    SearchService.ParseResult parseResult = searchService.parseQuery(ApplicationUsers.toDirectoryUser(rootUser), jqlSearch)

    if (!parseResult.isValid()) {
        println "INVALID jql Search: ${jqlSearch}"
        continue
    }

    SearchResults searchResult = searchService.search(ApplicationUsers.toDirectoryUser(rootUser), parseResult.getQuery(), PagerFilter.getUnlimitedFilter())
    if (searchResult?.total == 0) {
        println "NO issues assigned to user [${eachOldUser}]"
        continue
    }

    int userAssigneeChangeCounter = 0;

    for (Issue issue in searchResult.issues) {
        IssueInputParameters issueInputParameters = issueService.newIssueInputParameters();
        issueInputParameters.setAssigneeId(newUser.getKey());

        IssueService.UpdateValidationResult updateValidationResult = issueService.validateUpdate(rootUser.directoryUser, issue.id, issueInputParameters);
        if (updateValidationResult.isValid()) {
            issueService.update(rootUser.directoryUser, updateValidationResult);
            userAssigneeChangeCounter++
        } else {
            println "ERROR changing the assignee from [ ${eachOldUser} ] to [ ${newUser} ] for issue ${issue.id}"
        }
    }
    println "[${eachOldUser}] => [${newUser}] : ${userAssigneeChangeCounter} issues with assignee changed"
}